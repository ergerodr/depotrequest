package com.ergerodr.android.depotrequest;

import android.util.Log;

import com.google.api.client.extensions.android.http.AndroidHttp;
import com.google.api.client.googleapis.extensions.android.gms.auth.GoogleAccountCredential;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.services.sheets.v4.model.ValueRange;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by ergerodr on 9/6/2016.
 */
public class SheetsDownloader {
    private static final String TAG = "SheetsDownloader";
    private com.google.api.services.sheets.v4.Sheets mService;
    //'service' above can be converted to a local variable but would sacrifice readability....

    public com.google.api.services.sheets.v4.Sheets initAPI(GoogleAccountCredential credential){
        Log.d(TAG, "initAPI");
        HttpTransport transport = AndroidHttp.newCompatibleTransport();
        JsonFactory jsonFactory = JacksonFactory.getDefaultInstance();
        return mService = new com.google.api.services.sheets.v4.Sheets.Builder(
                transport, jsonFactory, credential)
                .setApplicationName("Google Sheets API Android Quickstart")
                .build();
    }
    /**
     * Fetch a list of names and majors of students in a sample spreadsheet:
     * https://docs.google.com/spreadsheets/d/1BxiMVs0XRA5nFMdKvBdBZjgmUUqptlbs74OgvE2upms/edit
     * @return List of names and majors
     * @throws IOException
     */
    public ArrayList<RequestTicket> getDataFromApi(com.google.api.services.sheets.v4.Sheets service) throws IOException {
        Log.d(TAG, "getDataFromApi");
        String spreadsheetId = "1bqq6xgaBPZOJTGg5e0s-nuznXu39sQgeW0GoOvlySdw";
        String range = "Imaging!B2:F";
        ArrayList<RequestTicket> tickets = new ArrayList<>();
        Log.d(TAG, "Before spreadsheet call");
        if(service == null) Log.d(TAG, "mService IS NULL");
        ValueRange response = service.spreadsheets().values()
                .get(spreadsheetId, range)
                .execute();
        Log.d(TAG, "After Spreadsheet call");
        List<List<Object>> values = response.getValues();
        if (values != null) {
            for (List row : values) {//For each list 'row' in values
                //NEED TO ADD TO ARRAYLIST FOR HOTSHEETFRAGMENT HERE.
                RequestTicket newTicket = new RequestTicket(
                        row.get(0).toString(),
                        row.get(4).toString());
                tickets.add(newTicket);
            }
        }
        //for(RequestTicket tik : tickets){
          //  Log.d("FOREACH", tik.getTicketNum() + tik.getTicketNotes());
        //}
        return tickets;
    }
}
