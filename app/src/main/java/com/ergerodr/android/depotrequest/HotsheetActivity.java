package com.ergerodr.android.depotrequest;

import android.support.v4.app.Fragment;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ergerodr on 9/6/2016.
 */
public class HotsheetActivity extends SingleFragmentActivity {
    private static final String TAG = "HotsheetActivity";
    private static final String EXTRA_IT_LIST_ID =
            "com.ergerodr.android.depotrequest.it_list_id";

    @Override
    protected Fragment createFragment(){
        Log.d(TAG, "createFragment");
        ArrayList<RequestTicket> tickets = getIntent().getParcelableArrayListExtra(EXTRA_IT_LIST_ID);
        return HotsheetFragment.newInstance(tickets);
    }
}
