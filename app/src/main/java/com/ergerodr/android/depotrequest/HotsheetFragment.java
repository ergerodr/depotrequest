package com.ergerodr.android.depotrequest;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Layout;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.google.android.gms.common.GoogleApiAvailability;
import com.google.api.client.extensions.android.http.AndroidHttp;
import com.google.api.client.googleapis.extensions.android.gms.auth.GoogleAccountCredential;
import com.google.api.client.googleapis.extensions.android.gms.auth.GooglePlayServicesAvailabilityIOException;
import com.google.api.client.googleapis.extensions.android.gms.auth.UserRecoverableAuthIOException;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.services.sheets.v4.model.ValueRange;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.io.IOException;
import java.util.List;

/**
 * Created by ergerodr on 9/2/2016.
 */
public class HotsheetFragment extends Fragment {
    private static final String TAG = "HotsheetFragment";
    private static final String ARG_TICKET_LIST = "DepotHotsheetItems";
    static final int REQUEST_GOOGLE_PLAY_SERVICES = 1002;

    private RecyclerView mHotsheet;
    private TicketAdapter mAdapter;
    private ArrayList<RequestTicket> mTickets;
    ProgressDialog mProgress;

    public static HotsheetFragment newInstance(ArrayList<RequestTicket> tickets){
        Bundle args = new Bundle();
        args.putParcelableArrayList(ARG_TICKET_LIST, tickets);
        HotsheetFragment fragment = new HotsheetFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onResume(){
        super.onResume();
        updateUI();
    }

    @Override
    public void onCreate(Bundle savedInstanceState){
        Log.d(TAG, "onCreate");
        super.onCreate(savedInstanceState);
        mProgress = new ProgressDialog(getContext());
        mTickets = getActivity().getIntent().getParcelableArrayListExtra(ARG_TICKET_LIST);
        for(RequestTicket tik : mTickets) Log.d(TAG, tik.getTicketNum() + " " + tik.getTicketNotes());
        if(mTickets == null) Log.d(TAG, "mTickets is NULL");
        if(mTickets != null) Log.d(TAG, "mTickets is NOT null");
        Log.d(TAG, "exiting onCreate");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState){
        Log.d(TAG, "onCreateView");
        View v = inflater.inflate(R.layout.fragment_hotsheet, container, false);

        mHotsheet = (RecyclerView) v.findViewById(R.id.hotsheetRecycleView);
        mHotsheet.setLayoutManager(new LinearLayoutManager(getActivity()));
        updateUI();
        return v;
    }

    private void updateUI(){
        Log.d(TAG, "updateUI");

        //Might need to create singleton class to manage RequestTickets or just
        //make another call to Sheets API to pull data again.
        if (mAdapter == null){
            mAdapter = new TicketAdapter(mTickets);
            mHotsheet.setAdapter(mAdapter);
        } else {
            mAdapter.setTicketItems(mTickets);
            mAdapter.notifyDataSetChanged();
        }
    }

    //////////////////////////RECYCLER-VIEW CODE/////////////////////////////


    //Indicates what elements a View in the recyclerView should have.
    private class TicketHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private RequestTicket mTicket;
        private TextView mTicketNumView;
        private TextView mNotesView;
        private Button mCallButton;
        private Button mInfoButton;

        //each View should have space for two buttons (call and more) A TextView that
        //makes the ticket number pop and the entire listItem should be clickable.
        public TicketHolder(final View ticketView){
            super(ticketView);
            ticketView.setOnClickListener(this);
            mTicketNumView = (TextView) ticketView.findViewById(R.id.tikNum);
            mNotesView = (TextView) ticketView.findViewById(R.id.notesView);
            mCallButton = (Button) ticketView.findViewById(R.id.callButton);
            mInfoButton = (Button) ticketView.findViewById(R.id.moreButton);
        }

        //Set each element to a concrete value in each list item in RecyclerView.
        public void bindTicket(final RequestTicket ticket){
            mTicket = ticket;
            mNotesView.setText(mTicket.getTicketNotes());
            mTicketNumView.setText(mTicket.getTicketNum());
            //get client name and set call button via implicit intent to call client
            //Use API for ITRequest to display more in-depth ticket details.
        }

        @Override
        public void onClick(View v){
            //Start new activity displaying more in-depth details.
        }
    }

    private class TicketAdapter extends RecyclerView.Adapter<TicketHolder> {
        private List<RequestTicket> mRequestTickets;

        public TicketAdapter (List<RequestTicket> tickets){ mRequestTickets = tickets; }

        @Override
        public TicketHolder onCreateViewHolder(ViewGroup parent, int viewType){
            LayoutInflater layoutInflater = LayoutInflater.from(getActivity());
            View view = layoutInflater.inflate(R.layout.ticket_list_item, parent, false);
            return new TicketHolder(view);
        }

        @Override
        public void onBindViewHolder(TicketHolder holder, int position){
            RequestTicket ticket = mRequestTickets.get(position);
            holder.bindTicket(ticket);
        }

        @Override
        public int getItemCount(){
            return mRequestTickets.size();
        }

        public void setTicketItems(List<RequestTicket> items) { mRequestTickets = items; }
    }
}
