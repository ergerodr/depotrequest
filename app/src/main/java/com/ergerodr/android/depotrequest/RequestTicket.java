package com.ergerodr.android.depotrequest;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

/**
 * Created by ergerodr on 9/2/2016.
 */
public class RequestTicket implements Parcelable {
    private static final String TAG = "RequestTicket";
    private String ticketNum;
    private String ticketNotes;

    public RequestTicket(String ticketNum, String ticketNotes){
        this.ticketNum = ticketNum;
        this.ticketNotes = ticketNotes;
    }
    //Constructor for Parcel
    private RequestTicket(Parcel in){
        ticketNum = in.readString();
        ticketNotes = in.readString();
    }

    public String getTicketNotes() {
        return ticketNotes;
    }

    public void setTicketNotes(String ticketNotes) {
        this.ticketNotes = ticketNotes;
    }

    public String getTicketNum() {
        return ticketNum;
    }

    public void setTicketNum(String ticketNum) {
        this.ticketNum = ticketNum;
    }

    //Defines the object we are going to parcel
    public int describeContents(){
        return hashCode();
    }
    //Object serialization. Each element must be serialized.
    public void writeToParcel(Parcel out, int flags){
        Log.d(TAG, "write to parcel..." + flags);
        out.writeString(ticketNum);
        out.writeString(ticketNotes);
    }
    //Required for de-serialization.
    public static final Parcelable.Creator<RequestTicket> CREATOR
            = new Parcelable.Creator<RequestTicket>(){

        public RequestTicket createFromParcel(Parcel in){
            return new RequestTicket(in);
        }

        public RequestTicket[] newArray(int size){
            return new RequestTicket[size];
        }
    };
}
